-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 07:44 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seminar`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` varchar(15) NOT NULL,
  `nama_peserta` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `id_seminar` varchar(15) NOT NULL,
  `seminar` varchar(50) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `jam_daftar` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `nama_peserta`, `email`, `id_seminar`, `seminar`, `tgl_daftar`, `jam_daftar`) VALUES
('ABS201908230001', 'Dian Fatimah', 'dianf@gmail.com', 'SM-01', 'SINAPTIKA 2019 SESI 01 (09:00-11:00)', '2019-08-01', '19:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_seminar`
--

CREATE TABLE `jadwal_seminar` (
  `id_seminar` varchar(15) NOT NULL,
  `nama_seminar` varchar(70) NOT NULL,
  `tgl_seminar` date NOT NULL,
  `jam_seminar` time NOT NULL,
  `durasi_seminar` time NOT NULL,
  `lokasi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_seminar`
--

INSERT INTO `jadwal_seminar` (`id_seminar`, `nama_seminar`, `tgl_seminar`, `jam_seminar`, `durasi_seminar`, `lokasi`) VALUES
('SM-01', 'SINAPTIKA 2019 SESI I (09:00 - 11:00)', '2019-08-22', '09:00:00', '02:00:00', 'Meruya'),
('SM-02', 'SINAPTIKA 2019 SESI II (13:00 - 15:00)', '2019-08-22', '13:00:00', '02:00:00', 'Meruya');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(15) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `jurusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `password`, `nama`, `jurusan`) VALUES
('41517110120', 'ibitganten', 'Dian Fatimah', 'Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `id_registrasi` int(11) NOT NULL,
  `nim` varchar(25) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(35) NOT NULL,
  `select_seminar` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`id_registrasi`, `nim`, `password`, `email`, `select_seminar`) VALUES
(1, '41517110123', 'Cobalagi', '', ''),
(3, '41517110123', 'Cobalagi', '', ''),
(4, '41517110123', 'h', '', ''),
(5, '41517110123', 'h', '', ''),
(6, '41517110123', 'h', '', ''),
(7, '41517110123', 'h', '', ''),
(8, '', '', '', ''),
(9, '', '', '', ''),
(10, '', '', '', ''),
(11, '', '', '', ''),
(12, '', '', '', ''),
(13, '', '', '', ''),
(14, '', '', '', ''),
(15, '41517110123', 'h', '', ''),
(16, '41517110123', 'h', '', ''),
(17, '41517110123', 'h', '', 'SINAPTIKA 2019 SESI II (13:00 - 15:00)'),
(18, '41517110123', 'dianf', '', '$row1'),
(19, '41517110123', 'dianf', '', '$row1'),
(20, '41517110123', 'rosyidmohammads@gmai', '', 'SINAPTIKA 2019 SESI II (13:00 - 15:00)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indexes for table `jadwal_seminar`
--
ALTER TABLE `jadwal_seminar`
  ADD PRIMARY KEY (`id_seminar`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`id_registrasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `id_registrasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
