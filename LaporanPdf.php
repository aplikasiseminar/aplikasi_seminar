<?php

class LaporanPdf extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('pdf');
        //load chart_model from model

        if ($this->session->userdata('status') != "login") {
            redirect(base_url("login"));
        } else {
            $this->load->model('m_data');
            $this->load->helper(array('form', 'url'));
        }
    }


    function index()
    {
        $pdf = new FPDF('l', 'mm', 'A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial', 'B', 16);
        // mencetak string
        $pdf->Cell(190, 7, 'Lamhot Sihaloho - 41517110125', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(190, 7, 'Daftar Mahasiswa', 0, 1, 'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(40, 6, 'NIM', 1, 0);
        $pdf->Cell(85, 6, 'Nama', 1, 0);
        $pdf->Cell(40, 6, 'Jurusan', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $mahasiswa = $this->m_data->tampil_data()->result();
        foreach ($mahasiswa as $row) {
            $pdf->Cell(40, 6, $row->nim, 1, 0);
            $pdf->Cell(85, 6, $row->nama, 1, 0);
            $pdf->Cell(40, 6, $row->jurusan, 1, 1);
        }
        $pdf->Output();
    }

//    function index()
//    {
//        $data = $this->chart_model->get_data()->result();
//        $x['data'] = json_encode($data);
//        $this->load->view('chart_view', $x);
//    }


    function line()
    {
        $data = $this->m_data->tampil_data()->result();
        $x['data'] = json_encode($data);
        $this->load->view('chart_view', $x);
    }


    function bar()
    {
        $data = $this->m_data->tampil_data()->result();
        $x['data'] = json_encode($data);
        $this->load->view('chart_view', $x);
    }
}
