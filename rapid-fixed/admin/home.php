<?php 
include '../configdb.php';

session_start();
 
if($_SESSION['status'] !="login"){
	header("location:../login-fix.php");
}

?>

<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<!-- Favicon -->
	<link href="../assets/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../css/font-awesome.min.css"/>
	<link rel="stylesheet" href="../css/slicknav.min.css"/>
	<link rel="stylesheet" href="../css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="../css/magnific-popup.css"/>
	<link rel="stylesheet" href="../css/animate.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="../css/style.css"/>

</head>
<body>
		<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		<div class="header-warp">
			<div class="header-social d-flex justify-content-end">
				<p>Follow us:</p>
				<a href="#"><i class="fa fa-pinterest"></i></a>
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-dribbble"></i></a>
				<a href="#"><i class="fa fa-behance"></i></a>
			</div>
				<a href="home.html" class="site-logo">
					<img src="././assets/img/logo.png" alt="">
				</a>
				<nav class="top-nav-area w-100">
					<div class="user-panel">
						<a href="logout.php">Logout</a>
					</div>
					<ul class="main-menu primary-menu">
						<li><a href="register.php">Registrasi</a></li>
						<li><a href="contact.html">My Barcode</a></li>
					</ul>
				</nav>
			</div>
	</header>
	<!-- Header section end -->

<!-- Hero section -->
	<section class="hero-section overflow-hidden">
		<div class="hero-slider owl-carousel">
			<div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="../img/slider-bg-1.jpg">
			</div>
			<div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="../img/slider-bg-2.jpg">
			</div>
			<div class="hero-item set-bg d-flex align-items-center justify-content-center text-center"data-setbg="../img/slider-bg-1.jpg">
			</div>
		</div>
	</section>
	<!-- Hero section end-->

	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.slicknav.min.js"></script>
	<script src="../js/owl.carousel.min.js"></script>
	<script src="../js/jquery.sticky-sidebar.min.js"></script>
	<script src="../js/jquery.magnific-popup.min.js"></script>
	<script src="../js/main.js"></script>
</body>
</html>